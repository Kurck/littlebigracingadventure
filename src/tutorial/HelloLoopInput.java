package tutorial;

import com.jme3.app.SimpleApplication;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseAxisTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;
 
/** Sample 4 - how to trigger repeating actions from the main update loop.
 * In this example, we make the player character rotate. */
public class HelloLoopInput extends SimpleApplication {
 
    public static void main(String[] args){
        HelloLoopInput app = new HelloLoopInput();
        app.start();
    }
 
    private float t = 0f;
    protected Geometry player, doub;
    private Material mat, mat1;
    private boolean isRunning = true;
 
    @Override
    public void simpleInitApp() {
 
        Box b = new Box(Vector3f.ZERO, 1, 1, 1);
        player = new Geometry("blue cube", b);
        mat = new Material(assetManager,
          "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.Blue);
        player.setMaterial(mat);
        player.setLocalTranslation(-2.0f, 0, 0);
        rootNode.attachChild(player);
        
        Box d = new Box(Vector3f.ZERO, 1, 1, 1);
        doub = new Geometry("red cube", d);
        mat1 = new Material(assetManager,
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat1.setColor("Color", ColorRGBA.Red);
        doub.setMaterial(mat1);
        doub.setLocalTranslation(2.0f, 0, 0);
        rootNode.attachChild(doub);

        initKeys(); // load my custom keybinding
    }
    
    /** Custom Keybinding: Map named actions to inputs. */
    private void initKeys() {
      // You can map one or several inputs to one named action
      inputManager.addMapping("Pause",  new KeyTrigger(KeyInput.KEY_P));
      inputManager.addMapping("Left",   new KeyTrigger(KeyInput.KEY_J));
      inputManager.addMapping("Right",  new KeyTrigger(KeyInput.KEY_K));
      inputManager.addMapping("Up",   new KeyTrigger(KeyInput.KEY_H), new MouseAxisTrigger(MouseInput.AXIS_WHEEL, false));
      inputManager.addMapping("Down",  new KeyTrigger(KeyInput.KEY_L), new MouseAxisTrigger(MouseInput.AXIS_WHEEL, true));
      inputManager.addMapping("Rotate", new KeyTrigger(KeyInput.KEY_SPACE),
                                        new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
      // Add the names to the action listener.
      inputManager.addListener(actionListener, new String[]{"Pause"});
      inputManager.addListener(analogListener, new String[]{"Left", "Right", "Up", "Down", "Rotate"});
   
    }
    
    private ActionListener actionListener = new ActionListener() {
      public void onAction(String name, boolean keyPressed, float tpf) {
        if (name.equals("Pause") && !keyPressed) {
          isRunning = !isRunning;
        }
      }
    };
    
    private AnalogListener analogListener = new AnalogListener() {
      public void onAnalog(String name, float value, float tpf) {
        if (isRunning) {
            Vector3f v = player.getLocalTranslation();
          if (name.equals("Rotate")) {
            player.rotate(0, value*speed*2, 0);
          } else if (name.equals("Right")) {
            player.setLocalTranslation(v.x + value*speed*4, v.y, v.z);
          } else if (name.equals("Left")) {
            player.setLocalTranslation(v.x - value*speed*4, v.y, v.z);
          } else if (name.equals("Up")) {
              player.setLocalTranslation(v.x, v.y + value*speed, v.z);
            } else if (name.equals("Down")) {
                player.setLocalTranslation(v.x, v.y - value*speed, v.z);
            }
        } else {
          //System.out.println("Press P to unpause.");
        }
      }
    };
 
    /* This is the update loop */
    @Override
    public void simpleUpdate(float tpf) {
        // make the player rotate
        //player.rotate(-2*tpf, 0, 0);
        //player.move(0, 0, -2*tpf);
    	if (isRunning) {
        doub.rotate(0, tpf, 0);
        t += tpf;
        doub.setLocalScale((float)Math.cos(t));
        if (((int)t) % 2 == 0)
        	doub.setMaterial(mat);
        else
        	doub.setMaterial(mat1);
    	}
    }
}