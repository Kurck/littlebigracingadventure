package tutorial;

import com.jme3.app.SimpleApplication;
import com.jme3.scene.VertexBuffer.Type;
import com.jme3.material.Material;
import com.jme3.math.FastMath;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Cylinder;
import com.jme3.scene.shape.Dome;
import com.jme3.scene.shape.PQTorus;
import com.jme3.scene.shape.Sphere;
import com.jme3.util.BufferUtils;
import com.jme3.math.ColorRGBA;

/**
 * Sample 2 - How to use nodes as handles to manipulate objects in the scene.
 * You can rotate, translate, and scale objects by manipulating their parent
 * nodes. The Root Node is special: Only what is attached to the Root Node
 * appears in the scene.
 */

public class KurckShape extends SimpleApplication {

	private static final float SPACING = 2.5f;
	private Vector3f loc = new Vector3f(-4 * SPACING, -2f, 0);

	public static void main(String[] args) {
		KurckShape app = new KurckShape();
		app.setShowSettings(false);
		app.start();
	}

	@Override
	public void simpleInitApp() {
		flyCam.setMoveSpeed(10);

		addGeometry("box", new Box(1, 1, 1), ColorRGBA.Blue);
		addGeometry("ball", new Sphere(16, 16, 1, false, true), ColorRGBA.Green);
		addGeometry("cylinder", new Cylinder(4, 16, 1, 2, true), ColorRGBA.Yellow);

		addGeometry("pyramid", new Dome(Vector3f.ZERO, 2, 4, 1f, false), ColorRGBA.Orange);
		addGeometry("cone", new Dome(Vector3f.ZERO, 2, 16, 1f, false), ColorRGBA.Red);
		addGeometry("hemisphere", new Dome(Vector3f.ZERO, 16, 16, 1f, false), ColorRGBA.Magenta);

		addGeometry("spiralTorus", new PQTorus(5, 3, 1f, .1f, 64, 16), ColorRGBA.Pink);
		addGeometry("flowerTorus", new PQTorus(3, 8, 1f, .1f, 64, 16), ColorRGBA.White);
		
		addGeometry("wobblySurface", getWobblySurface(.2f, 32), ColorRGBA.Brown);
	}


	private Mesh getWobblySurface(float amplitude, int detail) {
		Mesh mesh = new Mesh();
		float spacing = 1f / (float)detail;
		int vertCount = (detail + 1) * (detail + 1);
		Vector3f[] vertices = new Vector3f[vertCount];
		Vector2f[] texCoords = new Vector2f[vertCount];		
		int[] indices = new int[6 * detail * detail];
		for (int j = 0; j <= detail; j++) {
			for (int i = 0; i <= detail; i++) {
				// Vertex creation.
				int a = j * (detail + 1) + i;
				float x = i * spacing - .5f;
				float z = j * spacing - .5f;
				float y = amplitude * (FastMath.cos(8 * x) + FastMath.sin(16 * z)) / 2;
				vertices[a] = new Vector3f(x, y, z);
				texCoords[a] = new Vector2f(i, j); // Is this one done correctly?
				
				// Create groupings/triangles.
				if (!(i == detail || j == detail)) {
					int		b = a + 1,
							c = b + detail,
							d = c + 1,
							vi = 6 * (detail * j + i);
					indices[vi] = a;
					indices[vi + 1] = c;
					indices[vi + 2] = d;
					indices[vi + 3] = a;
					indices[vi + 4] = d;
					indices[vi + 5] = b;
				}
			}
		}
		
		// Construct the mesh
		mesh.setBuffer(Type.Position, 3, BufferUtils.createFloatBuffer(vertices));
		mesh.setBuffer(Type.TexCoord, 2, BufferUtils.createFloatBuffer(texCoords));
		mesh.setBuffer(Type.Index,    3, BufferUtils.createIntBuffer(indices));
		mesh.updateBound();
		return mesh;
	}

	private void addGeometry(String name, Mesh mesh, ColorRGBA colour) {
		Geometry g = new Geometry(name, mesh);

		// Set material
		//Material m = new Material(assetManager, "Common/MatDefs/Misc/ShowNormals.j3md");
		Material m = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		m.setColor("Color", colour);
		g.setMaterial(m);

		// Set location
		g.move(loc);
		loc.x += SPACING;

		// Add rotation controller
		g.addControl(new RotateControl());

		// Add to scene
		rootNode.attachChild(g);
	}

	/**
	 * A control for rotating objects automatically.
	 */
	private class RotateControl extends AbstractControl {

		private static final float ROT_SPEED = 0.6f;

		@Override
		public Control cloneForSpatial(Spatial spatial) {
			final RotateControl control = new RotateControl();
			control.setSpatial(spatial);
			return control;
		}

		@Override
		protected void controlUpdate(float tpf) {
			if (enabled && spatial != null) {
				spatial.rotate(0, ROT_SPEED * tpf, 0);
			}
		}

		@Override
		protected void controlRender(RenderManager rm, ViewPort vp) {
		}

	}

}