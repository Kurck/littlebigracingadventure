package menu;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.math.Vector2f;
import com.jme3.scene.Node;

public class MenuAppState extends AbstractAppState implements ActionListener {
	
	public enum MenuLayout {
		BASIC, SETTINGS, RACELIST;
		
		public Vector2f buttonSize = new Vector2f(500, 50);		// Size of buttons.
		public Vector2f spacing = new Vector2f(20, 20);			// Spacing in between buttons.
		public Vector2f screenEdge = new Vector2f(100, 100);	// Spacing between the edge of the screen and menu components.
		public Vector2f titleSpacing = new Vector2f(0, 100);	// Spacing between the title and the other menu components.
	}
	
    private SimpleApplication app;
    private Node menuNode;
    private boolean up, down, left, right, enter, back;
    private int selectedIndex, count = 1;
	
	@Override
	public void initialize(AppStateManager stateManager, Application app) {
		super.initialize(stateManager, app);
		this.app = (SimpleApplication)app;
		
		loadComponents();
		initKeys();
	}
	
	/**
	 * Loads the menu components such as buttons and corresponding actions.
	 */
	private void loadComponents() {
		// TODO: Attach all components to the menu node.
		// Sizes and spacing are based on the used layout.
		// Example: BASIC-layout; 3 buttons: Start, Settings, Quit; title: Little Big Racing Adventure
		this.menuNode.attachChild(createButton(new Vector2f(100f, 200f), "Start"));
		
		// Attach the menu node to the main gui node.
		this.app.getGuiNode().attachChild(this.menuNode);
	}
	
	private Node createButton(Vector2f position, String title) {
		Node n = new Node("button" + title);
		n.setLocalTranslation(position.x, position.y, 0f);
		return n;
	}

	/**
	 * Initialises and binds the keys.
	 */
	private void initKeys() {
		InputManager inputManager = this.app.getInputManager();
		
		// Map the inputs.
		inputManager.addMapping("Up", new KeyTrigger(KeyInput.KEY_UP), new KeyTrigger(KeyInput.KEY_W));
		inputManager.addMapping("Down", new KeyTrigger(KeyInput.KEY_DOWN), new KeyTrigger(KeyInput.KEY_S));
		inputManager.addMapping("Left", new KeyTrigger(KeyInput.KEY_LEFT), new KeyTrigger(KeyInput.KEY_A));
		inputManager.addMapping("Right", new KeyTrigger(KeyInput.KEY_RIGHT), new KeyTrigger(KeyInput.KEY_D));
		inputManager.addMapping("Enter", new KeyTrigger(KeyInput.KEY_RETURN));
		inputManager.addMapping("Back", new KeyTrigger(KeyInput.KEY_ESCAPE), new KeyTrigger(KeyInput.KEY_BACK));
		
		// Add the listener.
		inputManager.addListener(this, new String[]{"Up", "Down", "Left", "Right", "Enter", "Back"});
	}

	@Override
	public void update(float tpf) {
		super.update(tpf);
	}

	@Override
	public void onAction(String name, boolean isPressed, float tpf) {
		if (isPressed) { // Press
			if (name.equals("Up") && !up) { // cycle up
				up = true;
				selectedIndex++;
				if (selectedIndex >= count)
					selectedIndex = 0;
			} else if (name.equals("Down") && !down) { // cycle down
				down = true;
				selectedIndex--;
				if (selectedIndex < 0)
					selectedIndex = count - 1;
			} else if (name.equals("Back") && !back) { // go back to the previous menu or state 
				back = true;
				// TODO: perform back action
			} else {
				// TODO: ask selected component what to do.
			}
		} else { // Release
			if (name.equals("Up"))
				up = false;
			else if (name.equals("Down"))
				down = false;
			else if (name.equals("Back"))
				back = false;
			else if (name.equals("Enter"))
				enter = false;
			else if (name.equals("Left"))
				left = false;
			else if (name.equals("Right"))
				right = false;
		}
	}
}
