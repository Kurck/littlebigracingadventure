package menu;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

public class MenuController extends AbstractAppState implements ScreenController {

	private Nifty nifty;
	private Screen screen;
	private SimpleApplication app;
	
	// TODO: create style sheet for buttons if possible -> special buttons like those from LBA are hard to make in such stylesheets I think
	// TODO: (un)pause game when switching from/to menu
	public MenuController(AppStateManager stateManager, Application app) {
		initialize(stateManager, app);
	}
	
	@Override
	public void initialize(AppStateManager stateManager, Application app) {
		super.initialize(stateManager, app);
		this.app = (SimpleApplication)app;
	}
	
	@Override
	public void bind(Nifty nifty, Screen screen) {
		this.nifty = nifty;
		this.screen = screen;
	}

	@Override
	public void onEndScreen() { }

	@Override
	public void onStartScreen() { }
	
	@Override
	public void update(float tpf) {
		// TODO: update button looks
	}
	
	public void startGame() {
		//nifty.gotoScreen(hud);
		// load assets
		// unpause
	}
	
	public void gotoScreen(String screenName) {
		nifty.gotoScreen(screenName);
	}
	
	public void quitGame() {
		// TODO insert end screen
		app.stop();
	}
}
