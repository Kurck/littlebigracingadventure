package game;

import java.awt.DisplayMode;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.util.logging.Level;

import menu.MenuController;

import com.jme3.app.SimpleApplication;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;
import com.jme3.system.AppSettings;

import de.lessvoid.nifty.Nifty;

public class Game extends SimpleApplication {
	
	private static int BSC_SPEED = 2;
	private Geometry geom;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		java.util.logging.Logger.getLogger("").setLevel(Level.WARNING);
		
		AppSettings settings = new AppSettings(true);
		// TODO: load these from a config file-> settings.(load|save)("com.foo.LBRA");
		settings.setResolution(800,600);
		settings.setBitsPerPixel(24);
		settings.setFrameRate(60);
		settings.setFullscreen(false);
		settings.setSamples(2); // Anti-aliasing
		settings.setTitle("Little Big Racing Adventure");
		// TODO: load icons
		//settings.setIcons();
		settings.setSettingsDialogImage("Interface/splashscreen.jpg");
		
		Game app = new Game(); 
		app.setSettings(settings);
		app.setDisplayStatView(false);
		app.setDisplayFps(false);
		app.start();
	}

	@Override
	public void simpleInitApp() {
		getMenu();
		addBox();
	}

	private void getMenu() {
		NiftyJmeDisplay niftyDisplay = new NiftyJmeDisplay(assetManager, inputManager, audioRenderer, guiViewPort);
		/** Create a new NiftyGUI object */
		Nifty nifty = niftyDisplay.getNifty();
		/** Read your XML and initialize your custom ScreenController */
		//nifty.fromXml("Interface/mainMenu.xml", "start");
		nifty.fromXml("Interface/mainMenu.xml", "start", new MenuController(null, this));
		
		// attach the Nifty display to the gui view port as a processor
		guiViewPort.addProcessor(niftyDisplay);
		// disable the fly cam
		flyCam.setDragToRotate(true);
	}
	
	private void addBox() {
		Box b = new Box(Vector3f.ZERO, 1, 1, 1); // create cube shape at the origin
		this.geom = new Geometry("Box", b); // create cube geometry from the shape
		Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md"); // create a simple material
		mat.setColor("Color", ColorRGBA.Blue); // set color of material to blue
		this.geom.setMaterial(mat); // set the cube's material
		rootNode.attachChild(this.geom); // make the cube appear in the scene
	}

	@Override
	public void simpleUpdate(float tpf) {
		this.geom.rotate(BSC_SPEED * tpf, 0, BSC_SPEED * tpf);
	}
	
	/**
	 * Changes to full screen mode if possible.
	 */
	public void toggleToFullscreen() {
		GraphicsDevice device = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		DisplayMode[] modes = device.getDisplayModes();
		int i=0; // TODO: select correct mode
		settings.setResolution(modes[i].getWidth(),modes[i].getHeight());
		settings.setFrequency(modes[i].getRefreshRate());
		settings.setDepthBits(modes[i].getBitDepth());
		settings.setFullscreen(device.isFullScreenSupported());
		this.setSettings(settings);
		this.restart(); // restart the context to apply changes
	}

}
